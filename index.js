let block;

function goBack(){
    if(block=="block1"){
        document.getElementById("display1").style.display="none";
    }
    else if(block=="block2"){
        document.getElementById("display2").style.display="none";
    }
    else if(block=="block3"){
        document.getElementById("display3").style.display="none";
    }
    document.getElementById("btn").style.display="none";
    document.getElementById("search-grid").style.display="grid";
    block='';
}

function searchBySeasonEpisode(){
    let season=document.getElementById("season").value;
    let episode=document.getElementById("episode").value;
    document.getElementById("search-grid").style.display="none";
    document.getElementById("display1").style.display="grid";
    document.getElementById("btn").style.display="block";
    block="block1";
    let episodeList=data._embedded.episodes;
    let resultList = [];
    resultList=episodeList.filter( (epi) => epi.season==season && epi.number==episode);
    if(resultList.length==0){
        document.getElementById("display1").style.textAlign="center";
        document.getElementById("display1").innerHTML = "No results found!";
    }
    else{
        document.getElementById("display1").style.textAlign="left";
        document.getElementById("display1").innerHTML =
        "<div><a target='_blank' title='Click to know more' href='"+resultList[0].url+"'><img src='"+resultList[0].image.original+"'></a></div>"+
        "<div><b>Episode Name:</b> "+resultList[0].name+"</div>"+
        "<div><b>Summary:</b> "+resultList[0].summary+"</div>"+
        "<div><b>Number:</b> Season "+resultList[0].season+", Episode "+resultList[0].number+"</div>"+
        "<div><b>Airdate:</b> "+resultList[0].airdate+" at "+resultList[0].airtime+"</div>"+
        "<div><b>Runtime:</b> "+resultList[0].runtime+" minutes</div>";
    }
}

function searchByEpisodeIds(){
    let ids = document.getElementById("episodeId").value;
    let idList = ids.split(",");
    document.getElementById("search-grid").style.display="none";
    document.getElementById("display2").style.display="grid";
    document.getElementById("btn").style.display="block";
    block="block2";
    let episodeList=data._embedded.episodes;
    let resultList = [];
    document.getElementById("display2").innerHTML=''
    idList.forEach(id => {
        let tempList = [];
        tempList=episodeList.filter( (epi) => epi.id==id);
        console.log(tempList)
        resultList=resultList.concat(tempList);
        console.log(resultList)
    });
    if(resultList.length==0){
        document.getElementById("display2").style.textAlign="center";
        document.getElementById("display2").innerHTML = "No results found!"
    }
    else{
        if(resultList.length==1)
            document.getElementById("display2").className="width2"
        else
            document.getElementById("display2").className="width1"
        for(let i=0; i< resultList.length; i++){
            document.getElementById("display2").innerHTML+=
            "<div class='nested-grid'>"+
            "<div><a target='_blank' title='Click to know more' href='"+resultList[i].url+"'><img src='"+resultList[i].image.original+"'></a></div>"+
            "<div><b>Episode Name:</b> "+resultList[i].name+"</div>"+
            "<div><b>Summary:</b> "+resultList[i].summary+"</div>"+
            "<div><b>Number:</b> Season "+resultList[i].season+", Episode "+resultList[i].number+"</div>"+
            "<div><b>Airdate:</b> "+resultList[i].airdate+" at "+resultList[i].airtime+"</div>"+
            "<div><b>Runtime:</b> "+resultList[i].runtime+" minutes</div></div>";
        }
    }

}

function searchByEpisodeName(){
    let name = document.getElementById("episodeName").value;
    document.getElementById("search-grid").style.display="none";
    document.getElementById("display3").style.display="grid";
    document.getElementById("btn").style.display="block";
    block="block3";
    let episodeList=data._embedded.episodes;
    let resultList = [];
    document.getElementById("display3").innerHTML=''
    resultList=episodeList.filter( (epi) => epi.name.toLowerCase().includes(name.toLowerCase()));
    if(resultList.length==0){
        document.getElementById("display3").style.textAlign="center";
        document.getElementById("display3").innerHTML = "No results found!"
    }
    else{
        if(resultList.length==1)
            document.getElementById("display3").className="width2"
        else
            document.getElementById("display3").className="width1"
        for(let i=0; i< resultList.length; i++){
            document.getElementById("display3").innerHTML+=
            "<div class='nested-grid'>"+
            "<div><a target='_blank' title='Click to know more' href='"+resultList[i].url+"'><img src='"+resultList[i].image.original+"'></a></div>"+
            "<div><b>Episode Name:</b> "+resultList[i].name+"</div>"+
            "<div><b>Summary:</b> "+resultList[i].summary+"</div>"+
            "<div><b>Number:</b> Season "+resultList[i].season+", Episode "+resultList[i].number+"</div>"+
            "<div><b>Airdate:</b> "+resultList[i].airdate+" at "+resultList[i].airtime+"</div>"+
            "<div><b>Runtime:</b> "+resultList[i].runtime+" minutes</div></div>";
        }
    }
}

//Function to handle invalid inputs - Not implemented
// let season;
// let episode;

// function changeInput(){
    // let season=document.getElementById("season").value;
    // let episode=document.getElementById("episode").value;
    // if(season!='' && episode!=''){
    //     document.getElementById("submit").disabled=false;
    //     document.getElementById("error1").innerHTML="";
    //     document.getElementById("error2").innerHTML="";
    // }
    // else{
    //     document.getElementById("submit").disabled=true;
    //     if(season=='')
    //         document.getElementById("error1").innerHTML="Please enter season";
    //     else
    //         document.getElementById("error2").innerHTML="Please enter episode";
    // }
// }